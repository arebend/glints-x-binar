/*
 * f(s) = s * s
 * f(2) = 2 * 2
 * y = f(2)
 *
 * y = 4
 * */
/* This is a real function */
function f(s) {
    return s * s;
}

const y = f(2);
// console.log(y)

/* Procedure */
function greet() {
    // console.log("Hi, how are you?");
    // console.log("Where are you from?");
    // console.log("Nice to meet you!");
}

let x = greet();
// console.log(x);


// Therre are three ways to create of a function (FUNTION DECLARATION / SIGNATURE)

function functionName() {
    // DO SOMETHING
}

const anotherFuntinction = function () {
    // DO SOMETHING
}

// LAMDA
const arrowFuntion = () => {
    // DO SOMETHING
}

// LAMDA WITH PARAMS
const anotherFuntion = x => {
    console.log(x);
}

// LAMDA WITH PARAMS
const moreFuntion = (x, y) => {
    // DO SOMETHING
}

// ARROW FUNCTION VS FUNCTION KEYWORDS
function keywords() {
    console.log(this); // Global Object
}

const arrow = () => {
    console.log(this);
}
// arrow();


function circleArea(r) {
    return 3.14 * (r ** 2)
}

/*
 * Variable Scopes
 * And Function Procedure;
 * */

function tubeVolume(r, t) {
    // Local Variable
    let luasLingkaran = circleArea(r); // What variable is this?
    return luasLingkaran * t;
}

let result = tubeVolume(4, 5); // Global Variable
function more() {
    console.log(result);
}

function ant() {
    /*
     * What is Math.floor?
     * Find it on Google!
     * Keyword: How to create a random number in Javascript
     * */
    let bulat = Math.floor(result);
    console.log(bulat);
}




// FACTORY FUNCTION -> Deprecated / sudah usang

function Person(name, address) {
    this.name = name; // Instance Variable
    this.address = address;
}

// It is necessary to use function keyword, otherwise we can't call this
Person.prototype.greet = function () {
    console.log(`Hi, my name is ${this.name}`);
}

const Fikri = new Person("Fikri", "Solo");
Fikri.greet();
