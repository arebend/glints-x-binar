// // For Loop
// for (let i = 0; i < 100; i++) {
//   console.log("Hello World");
//   console.clear();
// }

// // While Loop
// let b = 100;
// while (b > 0) {
//   console.log('Hello World lagi');
//   b--;
// }

let arr = [
    "Apple",
    "Manggo",
    "Durian",
    "Orange",
    "Banana"
]

for (let i = 0; i < arr.length; i++) {
    if (arr[i] === "Durian") {
        console.log(arr[i] + ' is a stinky fruit')
    } else {
        // Apple is a delicious fruit
        console.log(arr[i] + ' is a delicious fruit');
    }
}

// Gimana caranya manggil elemen terakhir di dalam array
console.log(arr[arr.length - 1]);

arr.push("Tomato");
console.log(arr)

arr.pop(); // Remove the last element
arr.shift(); // Remove the first element

console.log(Math.floor(1.23123123));
console.log(Math.ceil(1.1231));

console.log(arr[Math.floor(Math.random() * (arr.length))])

let numbers = [1, 2, 3, 4, 5]
console.log(numbers.join(","));

let obj = {
    name: "Fikri Rahmat Nurhidayat",
    isMarried: false,
    address: {
        city: "Solo",
        province: "Jawa Tengah"
    }
}

console.log(obj);
console.log(obj.address.city);

for (let i in obj) {
    console.log('Keys:', i);
    console.log(obj[i]);

    // Nested Loop
    if (i == "address") {
        for (let b in obj[i]) {
            console.log(obj[i][b])
        }
    }
}