/*
  Number
    JS
      bigint
      double

    Another Language
      integer
      float
      double
      biginteger

  String
  Boolean
  Null
  Undefined

  Object
  Array
  Set
  Symbol
*/

// Number
10 // int
// => Oh, you need two locker your save your data
1029381092830921803982109809481092840981098230982109381209381928309213 // bigint
// => You need  10-16 locker to save your data
3.14 // float
3.1412798379827198 // double

// String
"Hello World";
"1234 Hello :0192$#!^@%@&^";
'This is a string with single quote and I can write double quotes inside here "';
"Write something \""

// Boolean
true
false
let a = 5;
let x = 10 === a;

let n = 10;
let m = "10";
console.log(n == m)

0, null, undefined, "", [], {} == false
10, "Content", [1], { key: "value" } == true

if (0) {
    console.log("Won't run")
}

if (true) {
    console.log("Test")
}
/*
  On/Off, conditinal checking
*/

// Null, undefined
let k;
console.log(k) // undefined
a = undefined;

let c = null;