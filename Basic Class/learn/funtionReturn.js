/*
  f(x) = x + 1;
  y = f(1);
*/

function calcCircleArea(r) {
    return 3.14 * (r ** 2)
}

// f(x) = 3.14 * (r**2) if you're using return
// f(x) = undefined

function tubeVolume(r, t) {
    let circleArea = calcCircleArea(r); // 3.14 * (r**2)
    return circleArea * t
}

// Rectangle Function
const calcRectangleArea = (l, w) => l * w;
const calcBlockVolume = (l, w, h) => {
    return calcRectangleArea(l, w) * h;
}