// It can be declaration etc.
let a = 10; // Variabel yang berubah, tetapi kita tidak bisa mendeklarasikan ulang nama Variabel itu.
const b = 18; // Immutable Variable -> Gak Bisa Berubah
var apa = 10;

let c; // Deklasrasi
c = 5; // Assignment sesuatu di dalam c dengan nilai 5
c = 10; // Assignment

var apa = "Hello"; // Redecralation and assign value
let x = b - 5;
// Output: a = 2

// Function signature
function test() {
    // console.log(this)

    // Jalanin sesuatu;
    var hasil = x - 2;
    // console.log(hasil);
}
test();

function luasSegitga(a, t) {
    // console.log(a * t / 2);
};

luasSegitga(12, 23);
// declaration dengan cara seperti ini
const namaFunction = function () {
    // onsole.log("Ngelakuin sesuatu");
}
namaFunction()

let h = function (contoh) {
    // console.log(contoh);
}
h("Contoh")
// Output: Contoh
// NOTE: Dua hal adalah sesuatu yang sama, namun beda dengan yang dibawah

// Arrow Function (Lamda)
const just = () => {
    console.log(this)
    // console.log("This is arrow function")
}
just();

// Naming Convention for function and variable
/*
  variable, let atau var => camelCase
  variable, const => SNAKE_CASE => #ffffff
  const WHITE = "#ffffff"
  const BLUE = "#f00"

  const calculator = require('./module.js')

  function => camelCase, it is recommended to not name function, more than three words
  calcCircleAreaAndGetTubeVolume => Forbidden
  calcTubeVolume => Correct
*/