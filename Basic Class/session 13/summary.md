### Object Oriented Programming (OOP):

# Daily Task #12

Create new classes who will inherit the abstract class called `Record`. There will be two classes:
* Product
  * name
  * price
  * stock
* Book
  * title
  * author
  * price
  * publisher

Use the abstraction concept to understand the concept of Record Class.

Your new class should be able to:
* Instantiate new data
* Saved to a file called `ClassName.json`

> HINT: You can just simply inherit record class and call save method
