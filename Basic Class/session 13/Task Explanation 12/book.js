const Record = require('./record');

module.exports = class Book extends Record {
    static properties = {
        title: {
            type: 'string',
            required: true
        },
        author: {
            type: 'string',
            required: true
        },
        price: {
            type: 'number',
            required: true
        },
        publisher: {
            type: 'string',
            required: true
        },
    }
}

// let book = new Book({
//     title: "Generasi 90an",
//     author: "Marchella Fp",
//     price: 125000,
//     publisher: "PT Kebahagiaan Itu Sederhana"
// })

// let book2 = new Book({
//     title: "Nanti Kita Cerita Tentang Hari Ini (NKCTHI)",
//     author: "Marchella Fp",
//     price: 99000,
//     publisher: "Kepustakaan Populer Gramedia"
// })

// book.save();
// book2.save();

// console.log(Book.find(1));

// console.log(book.delete(1));



