const Record = require('./record');

class Product extends Record {
    static properties = {
        name: {
            type: 'string',
            required: true
        },
        price: {
            type: 'number',
            required: true
        },
        stock: {
            type: 'number',
            required: true
        },
    }
}

let item = new Product({
    name: "Dress",
    price: 50000000,
    stock: 3
})
let item2 = new Product({
    name: "Pants",
    price: 20000000,
    stock: 4
})

module.exports = Product;

// item.save()
// item2.save()

