const Book = require('../Task Explanation 12/book');
const Product = require('../Task Explanation 12/product')


/* ==================== book.js ==================== */
let book = new Book({
    title: "Generasi 90an",
    author: "Marchella Fp",
    price: 125000,
    publisher: "PT Kebahagiaan Itu Sederhana"
})

let book2 = new Book({
    title: "Nanti Kita Cerita Tentang Hari Ini (NKCTHI)",
    author: "Marchella Fp",
    price: 99000,
    publisher: "Kepustakaan Populer Gramedia"
})

// book.save();
// book2.save();
/*
    let newbook = Book.find(1);
    console.log(newbook);
*/
// console.log(Book.find(1));
// console.log(Book.find(2));

// console.log(book.delete(1));
// console.log(book.delete(2));

Book.find(2).update()
// Book.find(2).delete()
/* Mix-ins => Create helper between class, usually it is being to help the class so it is not bloated */

/* ==================== product.js ==================== */
let item = new Product({
    name: "Dress",
    price: 50000000,
    stock: 3
})
let item2 = new Product({
    name: "Pants",
    price: 20000000,
    stock: 4
})

// item.save()
// item2.save()

// console.log(Product.find(1));
// console.log(Product.find(2));

// console.log(book.delete(1));
// console.log(book.delete(2));


