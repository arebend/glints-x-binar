const Record = require('./record');

class User extends Record {
    static properties = {
        email: {
            type: 'string',
            required: true,
            unique: true
        },
        encrypted_password: {
            type: 'string',
            required: true
        }
    }

    constructor(props) {
        super(props);
        this.password = this.encrypted_password;
    }

    set password(password) {
        return this.encrypted_password = `pretend-that-this-is-an-encrypted-version-of-${password}`
    }
}


let account = new User({
    email: "test01@mail.com",
    encrypted_password: "123456"
});

let account2 = new User({
    email: "test02@mail.com",
    encrypted_password: "654321"
});

let account3 = new User({
    email: "test01@mail.com",
    encrypted_password: "123456"
});

// account.save();
// account2.save();
// account3.save();



