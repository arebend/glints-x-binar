function beli(uang, obj) {
    return new Promise((resolve, reject) => {
        console.log(`Saya pergi membeli ${obj.item}`)
        setTimeout(function () {
            let kembalian = uang - obj.harga;
            if (kembalian > 0) {
                console.log(`Saya sudah membeli ${obj.item} uang kembaliannya ${kembalian}`);
                resolve(kembalian);
            } else {
                reject(`uang gk cukup nih buat beli ${obj.item}`);
            }
        });
    }, obj.waktu);
}

class Belanja {
    constructor(item, harga, waktu) {
        this.item = item;
        this.harga = harga
        this.waktu = waktu;
    }
}
let permen = new Belanja('permen', 2000, 2000);
let roti = new Belanja('roti', 20000, 4000);
let nasiPadang = new Belanja('nasi padang', 25000, 5000);
let kueCoklat = new Belanja('kue coklat', 40000, 1000);
let rokok = new Belanja('rokok', 21000, 2000);

let uang = 100000;
console.log('\n');

beli(uang, permen)
    .then(kembalian => {
        return beli(kembalian, roti);
    }).then(kembalian => {
        return beli(kembalian, nasiPadang);
    }).then(kembalian => {
        return beli(kembalian, kueCoklat);
    }).then(kembalian => {
        return beli(kembalian, rokok);
    }).catch(err => {
        console.log(err);
    });


