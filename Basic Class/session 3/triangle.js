// Triangle
/* PERIMETER OF TRIANGLE / KELILING SEGITIGA */

function calTrianglePerimeter(side) {
    if (typeof side !== "number") {
        throw new Error("input should be a number.")
    } else {
        return side + side + side
    }
}


/* AREA OF TRIANGLE / LUAS SEGITIGA */
function calTriangleArea(base, heigt) {
    if (typeof base !== "number" && typeof heigt !== "number") {
        throw new Error("input should be a number.")
    } else {
        return 0.5 * base * heigt
    }
}

try {
    let perimeter = calTrianglePerimeter('text')
    console.log(perimeter);
} catch (error) {
    console.log(error.message);
}
// console.log(calTrianglePerimeter(2)); // 6

try {
    let area = calTriangleArea(10, 8)
    console.log(area);
} catch (error) {
    console.log(error.message);
}
// console.log(calTriangleArea(10, 8)); // 40
