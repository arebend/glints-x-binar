// Tube
const readline = require("readline");

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

console.log(`What do you want to solve? 
1. Cylinder Volume
2. Cylinder Surface Area
`);

function cylinderVolume() {
    rl.question("Radius : ", r => {
        rl.question("Height : ", h => {
            // V = π*r^2*h
            console.log(`The volume of a cylinder with the radius ${r} cm and height ${h} cm is ${Math.PI * (r ** 2) * h} cm`);
            rl.close();
        })
    })
}

function cylinderSurfaceArea() {
    rl.question("Radius : ", r => {
        rl.question("Height : ", h => {
            // A = 2*π*r*h + 2*π*r^2
            console.log(`The surface area of a cylinder with the radius ${r} cm and height ${h} cm is ${(2 * Math.PI * r * h) + (2 * Math.PI * r * r)} cm`);
            rl.close();
        })
    })
}

function handleAnswer(a) {
    console.clear();
    switch (Number(a)) {
        case 1: cylinderVolume();
            break;

        case 2: cylinderSurfaceArea();
            break;

        default:
            console.log("Option is not available");
            break;
    }
}

rl.question("Answer : ", a => {
    handleAnswer(a);
})