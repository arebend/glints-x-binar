//	cube.js

//	@description	: calculate a cubical volume from user input
// 	@expected input : number
function calculateCubeVolume(s) {
    if (typeof s !== "number") {
        throw new Error("input should be a number.")
    } else {
        return s ** 3
    }
}

//	@description	: calculate a cubical surface area from user input
// 	@expected input : number
function calculateCubeArea(s) {
    if (typeof s !== "number") {
        throw new Error("input should be a number")
    } else {
        return 6 * (s ** 2)
    }
}

//	try-catch is used to test a sequence of instructions.
//	code will not stopped if the error exist, instead
// 	it will print the error message to the console.

try {
    console.log(calculateCubeArea("ok")); // "ok" arguments is not a number, and the function will throw an error
} catch (error) {						  // hence it will run the catch block.
    console.log(error.message)			  // "input should be a number" will be printed in console.
}

try {
    console.log(calculateCubeVolume(8));  // function will running and the result should be printed in console.	
} catch (error) {
    console.log(error.message)
}
