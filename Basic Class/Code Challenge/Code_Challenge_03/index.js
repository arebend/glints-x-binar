const products = require('./products.json');
const users = require('./user.json');

/*
    Code Challenge #3

    Your goals to create these endpoint
        /products
        This will show all products on products.json as JSON Response
    
        /products/available
        This will show the products which its stock more than 0 as JSON Response
    
        /users
        This will show the users data inside the users.json,
        But don't show the password!
*/

const http = require('http'); // Import HTTP module
const PORT = 4000;

/* Code Here */
function handler(req, res) {
    switch (req.url) {
        case '/':
            res.write("Welcome");
            break;

        case '/products':
            res.write(JSON.stringify(products));
            break;

        case '/products/available':
            let notEmptyProduct = []
            products.forEach(element => {
                if (element.stock >= 1) {
                    notEmptyProduct.push(element)
                }
            });
            res.write(JSON.stringify(notEmptyProduct));
            break;

        case '/users':
            delete users.password //Deleting Properties 
            res.write(JSON.stringify(users));
            break;

        default:
            res.write("404 Not Found!");
    }

    res.end();
}

const app = http.createServer(handler);

app.listen(4000, () => {
    console.log(`Listening on port ${PORT}`);
})