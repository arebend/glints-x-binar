const router = require('express').Router();

/* Core Module */
const userController = require('./controllers/user');
const postController = require('./controllers/post')


/* Middlewares */
const success = require('./middlewares/success');
const authenticate = require('./middlewares/authenticate');
const ownership = require('./middlewares/checkOwnership')

/* User Collection API */
router.post('/users/register', userController.register, success);
router.post('/users/login', userController.login, success);

/* Post Collection API */
router.post("/posts", authenticate, postController.create, success);
router.get("/posts", postController.findAll, success);

module.exports = router 
