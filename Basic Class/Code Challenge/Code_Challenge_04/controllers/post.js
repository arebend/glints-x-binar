const { Post } = require("../models");

exports.create = async function (req, res, next) {
    try {
        const post = await Post.create({
            title: req.body.title,
            body: req.body.body,
            user_id: req.user.id,
        });
        res.status(200);
        res.data = { post };
        next();
    } catch (err) {
        res.status(422);
        next(err);
    }
};

exports.findAll = async function (req, res, next) {
    try {
        const posts = await Post.findAll({
            include: "author",
        });
        res.status(200);
        res.data = { posts };
        next();
    } catch (err) {
        res.status(404);
        next(err);
    }
};


