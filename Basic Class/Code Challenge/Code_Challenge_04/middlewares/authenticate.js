const { User } = require('../models');

const jwt = require('jsonwebtoken');

module.exports = async (req, res, next) => {
    try {
        let token = req.headers.authorization;
        let payload = jwt.verify(token, process.env.SECRET_KEY);
        req.user = await User.findByPk(payload.id);
        next();
    }

    catch {
        res.status(401);
        next(new Error("You Token is Invalid"));
    }
}

