/* HOW TO RUN CODE : node <filename> test */

const data = require('./lib/arrayFactory.js');
const test = require('./lib/test.js');

/*
 * Code Here!
 * */
console.log();


// Optional
function clean(data) {
    return data.filter(i => typeof i === 'number');
}

// Should return array
function sortAscending(data) {
    // Code Here
    let dataNum = []
    for (let i = 0; i < data.length; i++) {
        if (data[i] != null) {
            dataNum.push(data[i])
        }
    }

    for (let i = 0; i < dataNum.length; i++) {
        for (let j = i + 1; j < dataNum.length; j++) {
            if (dataNum[i] > dataNum[j]) {
                let temp = dataNum[i]
                dataNum[i] = dataNum[j]
                dataNum[j] = temp
            }
        }
    }
    return dataNum;
}

// Should return array
function sortDecending(data) {
    // Code Here

    let dataNum = []
    for (let i = 0; i < data.length; i++) {
        if (data[i] != null) {
            dataNum.push(data[i])
        }
    }

    for (let i = 0; i < dataNum.length; i++) {
        for (let j = i + 1; j < dataNum.length; j++) {
            if (dataNum[i] < dataNum[j]) {
                let temp = dataNum[i]
                dataNum[i] = dataNum[j]
                dataNum[j] = temp
            }
        }
    }
    return dataNum;
}

// DON'T CHANGE
test(sortAscending, sortDecending, data);
