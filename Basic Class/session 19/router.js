const router = require('express').Router();

// Core Module
const userController = require('./controllers/userController');
const postController = require('./controllers/postController');

// Middleware
const success = require('./middlewares/success');
const authenticate = require('./middlewares/authenticate');
const checkOwnership = require('./middlewares/checkOwnership')

// User Collection API
router.post('/users/register', userController.register, success);
router.post('/users/login', userController.login, success);

// Product Collection API
router.get('/posts', postController.findAll, success);
// router.post('/posts', authenticate, checkOwnership('Post'), postController.create, success);
// router.put('/posts/:id', authenticate, checkOwnership('Post'), postController.update, success);

module.exports = router;
