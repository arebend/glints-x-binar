// Third Party Module
const express = require('express');
const morgan = require('morgan');

// Core Module
const exception = require('./middlewares/exception');
const router = require('./router');

require('dotenv').config();

const app = express();
const { PORT = 4000 } = process.env;

app.get('/', (req, res) => {
    res.status(200).json({
        status: 'success',
        message: "Hello World"
    })
})

/* Middleware to Parse JSON */
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use(morgan('dev'));
app.use('/api/v1', router);

// Apply Exception Handler
exception.forEach(handler =>
    app.use(handler)
);

app.listen(PORT, () => {
    console.log(`Server started at ${Date()}`);
    console.log(`Listening on port ${PORT}`);
})
