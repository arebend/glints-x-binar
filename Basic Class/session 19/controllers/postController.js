const { Post, User } = require('../models');

exports.create = async function (req, res, next) {
    try {
        const post = await Post.create({
            title: req.body.title,
            body: req.body.body,
            user_id: req.user.id
        });
        res.status(200);
        res.data = { post };
        next();
    }

    catch (err) {
        res.status(422);
        next(err);
    };
}

exports.findAll = async function (req, res, next) {
    try {
        const posts = await Post.findAll({
            include: 'author'
        })
        // let temp = []
        // for (let i = 0; i < posts.length; i++) {
        //     if (posts[i].author != posts[i].author.encrypted_password) {
        //         temp.push(posts[i].author)
        //     }
        // }
        // console.log(temp);

        res.status(200);
        res.data = { posts };
        next()

        
    }

    catch (err) {
        res.status(404);
        next(err);
    }
}

exports.update = async function (req, res, next) {
    /*
    Create authorization,
    so the other users who don't owned the Post
    can't update it 
    */
    try {
        await Post.update(req.body, {
            where: { id: req.params.id }
        });
        res.status(200);
        res.data = "Successfully updated!";
        next();
    }

    catch (err) {
        res.status(422);
        next(err);
    };
}

/* "encrypted_password": masih muncul */


// exports.delete = async function (req, res, next) {
//     // Finish this
//     try {
//         await Post.delete(req.body, {
//             where: { id: req.params.id }
//         })
//     } catch (err) {
//         res.status(422);
//         next(err);
//     }
// }
