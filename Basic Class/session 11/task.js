/* 
Requirements :
- Greet another Human
- Marry another Human
- Introduce themself
- Learn new language
- If person a tries to marry person b, make sure they've both understand the same language, otherwise, one of them will learn a new languange that both will understand.
*/

class Human {
    constructor(name, language) {
        this.name = name;
        this.language = language
    }

    greet(target) {
        console.log(`Hi ${target.name}! my name is ${this.name}!`);
    }

    introduce(target) {
        console.log(`Ohh.. Hi ${target.name}, my name is ${this.name}`);
    }

    learn(moreLanguage) {
        console.log(this.name, "before learn more Language: ", this.language);
        this.language.push(moreLanguage);
        console.log(this.name, "after learn more Language: ", this.language);
    }

    marry(Woman) {
        // console.log(this);
        // console.log(Woman);
        for (let i = 0; i < this.language.length; i++) {
            for (let j = 0; j < Woman.language.length; j++) {
                // console.log(this.language[i]);
                // console.log(Woman.language[j], 'Woman');
                if (this.language[i] == Woman.language[j]) {
                    console.log(`${this.name} is married with ${Woman.name}`);
                }
            }
        }
    }

}

let Man = new Human('Takada', ['Japan'])
let Woman = new Human('Sharon', ['English', 'Indonesia'])


Man.greet(Woman)
Woman.introduce(Man)
console.log('\n');

Woman.learn('Mandarin')
console.log('\n');

Man.learn('English')
console.log('\n');

Man.marry(Woman)