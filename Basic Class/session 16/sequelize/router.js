const router = require('express').Router();
const product = require('./controllers/productController');
const user = require('./controllers/userController');

/* product API Collection */
router.get('/products', product.all);
router.get('/product', product.show); // It could be merged in product.all handler
router.get('/products/available', product.available);
router.post('/products', product.create);
router.put('/products/edit/:id', product.update);
router.delete('/products/:id', product.delete);

/* user API Collection */
router.post('/users/register', user.register);
router.post('/users/login', user.login);
router.get('/users/me', user.me)

module.exports = router;
