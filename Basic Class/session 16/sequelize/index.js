const express = require('express');
const app = express()
const router = require('./router');
const dotenv = require('dotenv');
dotenv.config();

// Middleware to Parse JSON
app.use(express.json());
app.use(express.urlencoded({ extended: true }))

app.get('/', function (req, res) {
    res.json({
        status: true,
        message: 'Hello World'
    })
})

app.use('/', router);

app.listen(3000, () => {
    console.log("Listening on port 3000!");
})
