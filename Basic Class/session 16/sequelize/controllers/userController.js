const { user } = require('../models');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

module.exports = {
    register(req, res) {
        /*
            We can't save user's passwordas plain text
            We should encrypt that password
            before saving it to the DB.
            For safety reason.

            1. Data from User
            2. Change plain password into encrypted password using bcrypt
            3. Then save the result to the users table.
        */
        const salt = bcrypt.genSaltSync(10);
        const hash = bcrypt.hashSync(req.body.encrypted_password, salt); // TO ecnrypt the password

        user.create({
            email: req.body.email.toLowerCase(),
            encrypted_password: hash
        })
            .then(user => {
                res.status(201).json({
                    status: "success",
                    data: {
                        user
                    }
                })
            })
            .catch(err => {
                res.status(422).json({
                    status: "fail",
                    errors: [err.message]
                })
            })
    },

    /* 
        login(req, res) {
            // 1. Find the user by email [X]
            // 2. Get the user's encrypted_password property [X]
            // 3. Compare req.body.password with the encrypted_password 
            // ===================
            // 4. Create access token 
            // 5. Serve the response
            // ===================
        user.findOne({
            where: { email: req.body.email.toLowerCase() }
        })
            .then(user => {
                if (!user)
                    return res.status(401).json({
                        status: 'fail',
                        errors: ["Email doesn't exist!"]
                    })
                const isPasswordCorrect = bcrypt.compareSync(req.body.encrypted_password, user.encrypted_password);
                if (!isPasswordCorrect)
                    return res.status(401).json({
                        status: 'fail',
                        errors: ["Wrong password!"]
                    })
                res.status(201).json({
                    status: 'success',
                    message: "Succesfully logged in!",
                    data: {
                        user: user.entity
                    }
                })
            })
            .catch(err =>
                res.status(400).json({
                    status: 'fail',
                    errors: [err.message]
                })
            )
    }
    */
    login(req, res) {
        user.authenticate(req, body)
            .then(data => {
                res.status(201).json({
                    status: 'success',
                    data: data.entity
                })
            })
            .catch(err => {
                res.status(401).json({
                    status: 'fail',
                    errors: [err.message]
                })
            })
    },
    me(req, res) {
        let token = req.headers.authorization;
        if (!token) return res.status(401).json({
            status: 'fail',
            errors: ["Invalid Token"]
        })
        try {
            let payload = jwt.verify(token, process.env.SECRET_KEY);
        }
        catch (err) {
            return res.status(401).json({
                status: 'fail',
                errors: ["Invalid Token"]
            })
        }
        user.findByPk(payload.id)
            .then(user => {
                res.status(201).json({
                    status: 'success',
                    data: {
                        user
                    }
                })
            })
    }
}