const { product } = require('../models'); /* product is from model */
const { Op } = require('sequelize');

module.exports = {
    all(req, res) {
        product.findAll()
            .then((result) => {
                res.status(200).json({
                    status: 'success',
                    data: {
                        products: result
                    }
                })
            })
    },
    /* Example of using Query Params => http://localhost:3000/product?id=2 */
    show(req, res) {
        product.findOne({
            where: {
                id: req.query.id
            }
        })
            .then(product => {
                res.status(200).json({
                    status: 'success',
                    data: {
                        product
                    }
                })
            })
    },
    available(req, res) {
        product.findAll({
            where: {
                stock: {
                    [Op.gt]: 0
                }
            }
        })
            .then(products => {
                res.status(200).json({
                    status: 'success',
                    data: {
                        products
                    }
                })
            })
    },
    create(req, res) {
        product.create(req.body)
            .then((result) => {
                res.status(201).json({
                    status: "success",
                    data: {
                        product: result
                    }
                });
            })
            .catch((err) => {
                res.status(422).json({
                    status: 'fail',
                    errors: [err.message]
                });
            });
    },
    update(req, res) {
        /* Example of using Query Params */
        product.update(req.body, {
            where: {
                id: req.params.id /* :id */
            }
        })
            .then(data => {
                res.status(200).json({
                    status: "success",
                    message: `Product with ID ${req.params.id} is succesfully updated!`
                })
            })
            .catch(err => {
                res.status(422).json({
                    status: 'fail',
                    errors: [err.message]
                })
            })
    },
    delete(req, res) {
        product.destroy({
            where: {
                id: req.params.id
            }
        })
            .then(() => {
                res.status(204).json({
                    status: 'success',
                    message: `Product with ID ${req.params.id} is succesfully deleted!`
                })
            })
            .catch((err) => {
                res.status(422).json({
                    status: 'fail',
                    errors: [err.message]
                })
            });
    }
}