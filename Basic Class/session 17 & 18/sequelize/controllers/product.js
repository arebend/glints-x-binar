const { Product } = require('../models');
const { Op } = require('sequelize');
const successHandler = require('../middlewares/successHandlers');

module.exports = {
    getAall(req, res, next) {
        Product.findAll()
            .then((result) => {
                successHandler(res, 200, result, 'successfully get all product')
            })
    },
    /* Example of using Query Params => http://localhost:3000/product?id=2 */
    show(req, res) {
        Product.findOne({
            where: {
                id: req.query.id
            }
        })
            .then(product => {
                successHandler(res, 200, result, 'successfully get product using Query Params')
            })
    },
    getAvailable(req, res) {
        Product.findAll({
            where: {
                stock: {
                    [Op.gt]: 0
                }
            }
        })
            .then(products => {
                successHandler(res, 200, result, 'successfully get all available product')
            })
    },
    create(req, res) {
        Product.create(req.body)
            .then((result) => {
                successHandler(res, 201, result, 'successfully create product')
            })
            .catch((err) => {
                res.status(422).json({
                    status: 'fail',
                    errors: [err.message]
                });
            });
    },
    getOne(req, res) {
        Product.findByPk(req.params.id)
            .then((result) => {
                if (result) {
                    successHandler(res, 201, result, 'successfully get one product')
                } else {
                    res.status(404).json({
                        status: 'not success find data',
                        message: `Data product not found`
                    })
                }
            }).catch((err) => {
                res.status(500).json({
                    message: `Internal server ${err}`
                })
            });
    },
    update(req, res) {
        /* Example of using Query Params */
        Product.update(req.body, {
            where: {
                id: req.params.id /* :id */
            }
        })
            .then(data => {
                successHandler(res, 200, result, `Product with ID ${req.params.id} is succesfully updated!`)
            })
            .catch(err => {
                res.status(422).json({
                    status: 'fail',
                    errors: [err.message]
                })
            })
    },
    delete(req, res) {
        Product.destroy({
            where: {
                id: req.params.id
            }
        })
            /* Delete tidak menampilkan message, coba cek DB karena data hilang */
            .then(() => {
                successHandler(res, 204, result, `Product with ID ${req.params.id} is succesfully updated!`)
            })
            .catch((err) => {
                res.status(422).json({
                    status: 'fail',
                    errors: [err.message]
                })
            });
    }
}