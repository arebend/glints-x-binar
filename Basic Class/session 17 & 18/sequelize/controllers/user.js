const { User } = require('../models');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

module.exports = {
    register(req, res) {
        User.create({
            email: req.body.email,
            encrypted_password: req.body.password
        })
            .then(user => {
                res.status(201).json({
                    status: 'success',
                    data: { user }
                })
            })
            .catch(err => {
                res.status(422).json({
                    status: "fail",
                    errors: [err.message]
                })
            })
    },

    login(req, res) {
        User.findOne({
            where: { email: req.body.email }
        })
            .then((user) => {
                console.log(user.dataValues);
                let token = jwt.sign({ id: user.id, email: user.email }, process.env.SECRET_KEY)
                if (user && bcrypt.compareSync(req.body.password, user.encrypted_password)) {
                    res.status(201).json({
                        token
                    })
                } else {
                    res.status(400).json({
                        message: 'incorect email or password'
                    })
                }
            })
            .catch((err) => {
                return res.status(401).json({
                    status: 'fail',
                    errors: [err.message]
                })
            });
    },
    me(req, res) {
        res.status(201).json({
            status: 'success',
            data: {
                User
            }
        })
    }
}