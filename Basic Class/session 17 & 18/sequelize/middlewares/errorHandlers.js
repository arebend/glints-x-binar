const errorHandler = [
    (err, req, res, next) => {

        if (res.statusCode === 200) {
            res.status(500).json({
                status: 'fail',
                error: [err.message]
            })
        }
    },

    (req, res, next) => {
        const err = new Error('Not Found')
        res.status(404)
        next(err)
    }
]

module.exports = errorHandler

