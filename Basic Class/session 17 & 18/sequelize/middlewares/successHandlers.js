const SuccessHandlers = (res, code, result, msg) => {
    switch (code) {
        case 200:
            return res.status(code).json({
                status: 'Success',
                msg,
                result
            })
        case 201:
            return res.status(code).json({
                status: 'Success',
                msg,
                result
            })
        case 204:
            return res.status(code).json({
                status: 'Success',
                msg,
                result
            })
        default:
            console.log('status code not found');
            break;
    }
}

module.exports = SuccessHandlers
