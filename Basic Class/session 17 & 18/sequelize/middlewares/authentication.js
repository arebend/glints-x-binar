const jwt = require('jsonwebtoken');
const { User } = require('../models')

module.exports = async (req, res, next) => {
    /* Share the same req and res object. */
    try {
        let token = req.headers.access_token;
        let payload = await jwt.verify(token, process.env.SECRET_KEY);
        // console.log(token);
        req.User = await User.findByPk(payload.id)
        next();
    }
    catch (err) {
        res.status(401);
        next(new Error("Invalid Tokennnn"));
    }
}