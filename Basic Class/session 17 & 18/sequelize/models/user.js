'use strict';
const bcrypt = require('bcryptjs');

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {

    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        isEmail: true,
        notEmpty: true
      }
    },

    encrypted_password: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: true,
        min: 6,
        notEmpty: true
      },
    }

  }, {
    hooks: {
      beforeValidate: instance => {
        instance.email = instance.email.toLowerCase();
      },
      beforeCreate(instance) {
        const salt = bcrypt.genSaltSync(10);
        const hash = bcrypt.hashSync(instance.encrypted_password, salt);
        instance.encrypted_password = hash
      }
    }
  });
  User.associate = function (models) {
    // associations can be defined here
  };
  return User;
};