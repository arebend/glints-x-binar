const router = require('express').Router();
const product = require('../controllers/product');
const authenticate = require('../middlewares/authentication')


/* GET data Products */
router.get('/', product.getAall)
router.get('/available', product.getAvailable)
router.get('/:id', product.getOne)

router.get('/product', product.show); // It could be merged in product.all handler

/* CREATE data PRODUCT */
router.post('/create', authenticate, product.create)

/* Update data PRODUCT */
router.put('/update/:id', authenticate, product.update)

/* Delete data PRODUCT */
router.delete('/delete/:id', authenticate, product.delete)

module.exports = router
