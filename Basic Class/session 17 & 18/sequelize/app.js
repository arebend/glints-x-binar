const express = require('express');
const app = express();
const morgan = require('morgan');
const cors = require('cors');

/* Server Configuration */
const dotenv = require('dotenv');
dotenv.config(); // Read .env and set it as Environment Variable

const router = require('./routers');
const errorHandler = require('./middlewares/errorHandlers');

/* Middleware to Parse JSON */
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(morgan('tiny'));
app.use(cors()); // CORS to allow front-end use our server

app.get('/', (req, res) => { res.json({ status: true, message: 'Hello World' }) })


app.use(router);

/* errorHandler Handler Must Be Bellow the router */
errorHandler.forEach(handler => app.use(handler))

app.listen(3000, () => { console.log("Listening on port 3000!"); })
