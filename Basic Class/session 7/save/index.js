const readline = require('readline'); // read line module
const fs = require('fs'); // path module
const path = require('path')// path module

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.question("What is your email? ", email => {
    rl.question("What is your password", password => {
        register(email, password)

        rl.close()
    })
});

// __dirname
// __dirname is consistent

function register(email, password) {
    fs.writeFileSync(
        path.resolve(__dirname, '.', 'data.json'), //path module
        JSON.stringify({
            email, password
        }, null, 2)) //change data into string, with JSON format
}

rl.on('close', () => {
    process.exit
});