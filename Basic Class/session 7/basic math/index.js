// Import Readline Module
// Node JS,  Common JS
const readline = require("readline");
const square = require("./square.js");

//Create interface
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

// To ask user an input will be used on area function

function getAreaInput() {
    rl.question("Side :", answer => {
        console.log(
            "Here is the result",
            square.area(+answer)
        );
        rl.close();
    })
}

// To ask user an input will be used on round function
function getRoundInput() {
    rl.question("Side :", answer => {
        console.log(
            "Here is the result",
            square.round(+answer)
        );
        rl.close();
    })
}

console.clear(); // To clear up the console
console.log(`Which operation do you want to do?
1. Calculate Square Area
2. Calculate Square Round
`);

function handleAnswer(answer) {
    console.clear();
    switch (+answer) {
        case 1: return getAreaInput();
        case 2: return getRoundInput();
        default:
            console.log("Options is not avaliable");
            rl.close();
    }
}

rl.question("Answer", answer => {
    handleAnswer(answer)
});

rl.on("close", () => {
    process.exit;
});