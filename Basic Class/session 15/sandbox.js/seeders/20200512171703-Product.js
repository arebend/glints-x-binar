'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      */
    return queryInterface.bulkInsert('Products', [{
      name: 'Pensil',
      price: 4000,
      stock: 14,
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      name: 'Penghapus',
      price: 2000,
      stock: 8,
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      name: 'Buku',
      price: 5000,
      stock: 6,
      createdAt: new Date(),
      updatedAt: new Date(),

    }], {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      */
    return queryInterface.bulkDelete('Products', null, {});
  }
};
