const router = require('express').Router();
const Product = require('../controller/Product');

//  GET data Products
router.get('/', Product.getAll)
router.get('/available', Product.getAvailable)

//  CREATE data PRODUCT
router.post('/create', Product.createProduct)

//  Update data PRODUCT
router.put('/update/:id', Product.updateProduct)

//  Delete data PRODUCT
router.delete('/delete/:id', Product.deleteProduct)

module.exports = router
