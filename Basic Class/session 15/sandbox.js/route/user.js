const router = require('express').Router();
const User = require('../controller/user');

router.get('/', User.login)

module.exports = router
