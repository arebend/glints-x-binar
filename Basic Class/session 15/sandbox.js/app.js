'use strict';

const express = require('express')
const app = express();
const port = 3000;
const cors = require('cors');
const router = require('./route');

// Middleware
app.use(express.json()); // JSON parser
app.use(cors()); // CORS to allow front-end use our server

app.use(router);

// GET /
app.get('/', (req, res) =>
    res.json({
        status: true,
        message: "Hello World!"
    })
);


app.listen(port, () =>
    console.log(`Example app listening at http://localhost:${port}`)
)