### General Descripton
 - SQL : a place when we store the records
 - DBMS (data base management system) : the way we store the record in database

```
 SQLite : game offline, mobile
 MariaDB : desktop app
 PostgresQL : lebih advance dari MySQL
```

```
CREATE USER 'whoami' WITH PASSWORD 'password' SUPERUSER; 

 CREATE TABLE users (id BIGSERIAL NOT NULL PRIMARY KEY, name VARCHAR(100) NOT NULL, email VARCHAR(200) NOT NULL); 

 INSERT INTO users (email, encrypted_password) VALUES ('test01@mail.com', 'pretend-that-this-is-an-encrypted-version-of-123456'); 

SELECT * FROM users;

DELETE FROM users WHERE users.id = 5 (the id that you want to deleted)

UPDATE users SET verified = true WHERE user.id = 2 (the id that you want to deleted)

 Add new column : ALTER TABLE users ADD COLUMN verified BOOLEAN DEFAULT FALSE; 

ALTER TABLE users DROP COLUMN random; 
 ```



 ```
 - sequelize init -y
 - npm install package_name (sequelize, express, cors, pg, pg-hstore)
 - sequelize db:create
 - sequelize model:generate --name User --attributes email:string,encrypted_password:string,verified:boolean --underscored true
 - sequelize model:generate --name Product --attributes name:string,price:integer,stock:integer
 - sequelize db:migrate
 ```

