const fs = require('fs');

class Record {
    constructor(props) {
        if (this.constructor == Record)
            throw new Error("Can't instantiate from Record");

        this._validate(props);
        this._set(props);
    }

    _validate(props) {
        if (typeof props !== 'object' || Array.isArray(props))
            throw new Error("Props must be an object");

        this.constructor.properties.forEach(i => {
            if (!Object.keys(props).includes(i))
                throw new Error(`${this.constructor.name}: ${i} is required`)
        })
    }

    _set(props) {
        this.constructor.properties.forEach(i => {
            this[i] = props[i];
        })
    }

    get all() {
        try {
            return eval(
                fs.readFileSync(`${__dirname}/${this.constructor.name}.json`)
                    .toString()
            )
        }
        catch {
            return []
        }
    }

    find(id) {

    }

    update(id) {

    }

    delete(id) {

    }

    save() {
        fs.writeFileSync(
            `${__dirname}/${this.constructor.name}.json`,
            JSON.stringify([...this.all, { id: this.all.length + 1, ...this }], null, 2)
        );
    }
}

/* ==================== USER ==================== */
class User extends Record {
    static properties = [
        "email",
        "password"
    ]
}
let Fikri = new User({
    email: "test01@mail.com",
    password: "123456"
});
let Fikri2 = new User({
    email: "test02@mail.com",
    password: "654321"
});

/* ==================== BOOK ==================== */
class Book extends User {

    static properties = [
        "title",
        "author",
        "price",
        "publisher"
    ]
}
let book = new Book({
    title: "Generasi 90an",
    author: "Marchella Fp",
    price: 125000,
    publisher: "PT Kebahagiaan Itu Sederhana"
})
let book2 = new Book({
    title: "Nanti Kita Cerita Tentang Hari Ini (NKCTHI)",
    author: "Marchella Fp",
    price: 99000,
    publisher: "Kepustakaan Populer Gramedia"
})

/* ==================== PRODUCT ==================== */
class Product extends Record {
    static properties = [
        "name",
        "price",
        "stock"
    ]
}
let item = new Product({
    name: "Dress",
    price: 50000000,
    stock: 3
})
let item2 = new Product({
    name: "Pants",
    price: 20000000,
    stock: 4
})


console.log(book);
console.log(book2);
console.log(item);
console.log(item2);
console.log(Fikri);
console.log(Fikri2);

// Fikri.save();
// Fikri2.save();
// book.save()
// book2.save()
// item.save()
// item2.save()

/*
  Make two class who inherit Abstract Class called Record

  Book,
    title
    author
    price
    publisher

  Product,
    name,
    price,
    stock
*/
