## Daily Task #14

### Create an API that which has these endpoint

- POST /calculate/circleArea
- POST /calculate/squareArea
- POST /calculate/cubeVolume
- POST /calculate/tubeVolume
- POST /calculate/triangleArea
- GET  /info

### GET /info
This endpoint will return what calculation this API support as a response
```{
  "status": true,
  "data": [
    "POST /calculate/circleArea",
    ...
  ]
}
```

### Do's and Don't
Don't work with just one file, seperate your code in another file for better readability
Add CORS

### Reference
- https://www.npmjs.com/package/express
- https://expressjs.com/
- https://www.youtube.com/watch?v=L72fhGm1tfE
- https://www.youtube.com/watch?v=QO4NXhWo_NM&feature=youtu.be
- https://developer.mozilla.org/en-US/docs/Web/HTTP/Status