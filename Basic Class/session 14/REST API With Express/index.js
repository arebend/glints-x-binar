'use strict';

const express = require('express')
const app = express();
// const cors = require('cors');
const port = 3000;

const router = require('./router');

// Middleware
app.use(express.json()); // JSON parser
// app.use(cors()); // CORS to allow front-end use our server

// GET /
app.get('/', (req, res) =>
    res.json({
        status: true,
        message: "Hello World!"
    })
);

app.use('/', router);

app.listen(port, () =>
    console.log(`Example app listening at http://localhost:${port}`)
)