const area = require('../db/area.json')

module.exports = {
    getSquareArea: (req, res) => {
        // console.log(area);
        res.status(201).json(area.filter(i => i.name == "square"))
    },
    postSquareArea: (req, res) => {
        // console.log(req.body);
        res.status(201).json({
            name: req.body.name,
            side: req.body.side,
            result: req.body.side * req.body.side
        })
    }
}