const area = require('../db/area.json')

module.exports = {
    getCircleArea: (req, res) => {
        // console.log(area);
        res.status(201).json(area.filter(i => i.name == "circle"))
    },
    postCircleArea: (req, res) => {
        res.status(201).json({
            name: req.body.name,
            radius: req.body.radius,
            result: req.body.radius * Math.PI
        })
    },
}
