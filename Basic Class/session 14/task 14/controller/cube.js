const area = require('../db/volume.json')

module.exports = {
    getCubeVolume: (req, res) => {
        // console.log(area);
        res.status(201).json(area.filter(i => i.name == "cube"))
    },
    postCubeVolume: (req, res) => {
        res.status(201).json({
            name: req.body.name,
            edge: req.body.edge,
            result: req.body.edge ** 3
        })
    }
};