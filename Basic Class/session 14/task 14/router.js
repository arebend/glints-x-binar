// Route Level Middleware
const router = require('express').Router()

// Import Controller
const { getCircleArea, postCircleArea } = require('./controller/circle')
const { getSquareArea, postSquareArea } = require('./controller/square')
const { getCubeVolume, postCubeVolume } = require('./controller/cube')
// const { getTubeVolume, postTubeVolume } = require('./controller/tube')
// const { getTriangleArea } = require('./controller/triangle')


router.get('/calculate/circleArea', getCircleArea)
router.post('/calculate/circleArea', postCircleArea)

router.get('/calculate/squareArea', getSquareArea)
router.post('/calculate/squareArea', postSquareArea)

router.get('/calculate/cubeVolume', getCubeVolume)
router.post('./calculate/cubeVolume', postCubeVolume)

// router.get('/calculate/tubeVolume', getTubeVolume)
// router.post('./calculate/tubeVolume', postTubeVolume)

// router.post('./calculate/triangleArea', getTriangleArea)


module.exports = router
