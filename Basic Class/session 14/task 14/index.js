'use strict';

const express = require('express')
const cors = require('cors');

const app = express();
const port = 4000;

const router = require('./router');

// Middleware
app.use(express.json()); // JSON parser
app.use(cors()); // CORS to allow front-end use our server

// GET
app.get('/', (req, res) =>
    res.json({
        status: true,
        message: "Hello World!"
    })
);

// GET /info
app.get('/info', (req, res) => {
    res.status(200).json({
        "status": true,
        "data": [
            "POST /calculate/circleArea",
            "POST /calculate/squareArea",
            "POST /calculate/triangleArea",
            "POST /calculate/cubeVolume",
            "POST /calculate/tubeVolume",
        ]
    })
})

app.use('/', router);

app.listen(port, () =>
    console.log(`Example app listening at http://localhost:${port}`)
)